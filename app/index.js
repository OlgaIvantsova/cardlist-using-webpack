import 'bootstrap/less/bootstrap.less';
import './styles.less';
import ng from 'angular';
import uirouter from 'angular-ui-router';
import routing from './router-config';
import Components from './components';

export default ng.module('app', [uirouter, Components])
                 .config(routing)
                 .name;
