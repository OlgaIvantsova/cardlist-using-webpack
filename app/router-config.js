export default function routing($urlRouterProvider, $stateProvider) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $stateProvider
  .state('app', {
    url: '/',
    template: '<home-page></home-page>',
  })
  .state('app.categories', {
    url: '/:variable',
    template: '<cards-list category="category"></cards-list>',
  });
}
