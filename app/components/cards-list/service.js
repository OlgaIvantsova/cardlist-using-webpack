export default class CardsService {
  constructor($http) {
    'ngInject';

    this.$http = $http;
  }

  getCards() {
    return this.$http.get(`${process.env.PORT}cards`).then((response) => {
      return response.data;
    });
  }

  list(categoryId) {
    if (!categoryId) {
      return this.getCards();
    }
    return this.$http.get(`${process.env.PORT}cards?categoryId=${categoryId}`).then((response) => {
      return response.data;
    });
  }

  add(card) {
    return this.$http.post(`${process.env.PORT}cards`, card).then((response) => {
      return response.data;
    });
  }

  remove(id) {
    return this.$http.delete(`${process.env.PORT}cards/${id}`);
  }

}
