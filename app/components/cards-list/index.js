import ng from 'angular';
import CardsComponent from './component';
import CardsService from './service';

export default ng.module('app.components.cards-list', [])
                 .service('CardsService', CardsService)
                 .component('cardsList', CardsComponent)
                 .name;
