export default class CardsController {
  constructor(CardsService) {
    'ngInject';

    this.cardsService = CardsService;
  }

  $onInit() {
    this.cardsService.list(this.category.id).then((value) => {
      this.cards = value;
    });
  }

  add() {
    const card = {
      title: this.title,
      categoryId: this.category.id,
    };
    this.cardsService.add(card).then((value) => {
      this.$onInit();
      this.title = '';
      return value;
    });
  }

  remove(id) {
    const isAgree = confirm('Do you really want to remove this card?');
    if (isAgree) {
      this.cardsService.remove(id).then(() => {
        this.$onInit();
      });
    }
  }

}
