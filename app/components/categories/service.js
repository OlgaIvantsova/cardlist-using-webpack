export default class CategoriesService {
  constructor($http) {
    'ngInject';

    this.$http = $http;
  }

  list() {
    return this.$http.get(`${process.env.PORT}categories`).then((request) => {
      return request.data;
    });
  }

  detail(name) {
    return this.$http.get(`${process.env.PORT}categories?title=${name}`).then((request) => {
      return request.data;
    });
  }

  add(category) {
    return this.$http.post(`${process.env.PORT}categories`, category).then((request) => {
      return request.data;
    });
  }
}
