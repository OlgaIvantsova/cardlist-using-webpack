import ng from 'angular';
import CategoriesComponent from './component';
import CategoriesService from './service';

export default ng.module('app.components.categories', [])
                 .service('CategoriesService', CategoriesService)
                 .component('categories', CategoriesComponent)
                 .name;
