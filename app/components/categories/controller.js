export default class CategoriesController {
  constructor(CategoriesService) {
    'ngInject';

    this.categoriesService = CategoriesService;
  }

  $onInit() {
    this.curCategory = '';
    this.newCategory = '';
    this.list();
  }

  list() {
    this.categoriesService.list().then((value) => {
      this.categories = value;
      this.filteredCategories = value;
    });
  }

  setCategory(title) {
    this.curCategory = title;
    if (this.curCategory === 'all') {
      this.filteredCategories = this.categories;
      return;
    }
    this.categoriesService.detail(this.curCategory).then((value) => {
      this.filteredCategories = value;
    });
  }

  addCategory() {
    const category = {
      title: this.newCategory,
    };
    this.categoriesService.add(category).then(() => {
      this.list();
      this.newCategory = '';
    });
  }

}
