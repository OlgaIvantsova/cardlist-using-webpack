import ng from 'angular';
import PageComponent from './component';

export default ng.module('app.components.homepage', [])
                 .component('homePage', PageComponent)
                 .name;
