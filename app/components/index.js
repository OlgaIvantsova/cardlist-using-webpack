import ng from 'angular';
import HomePage from './home-page';
import Categories from './categories';
import CardsList from './cards-list';
import Card from './card';

export default ng.module('app.components', [HomePage, Categories, CardsList, Card])
                 .name;
