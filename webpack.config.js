const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const definePlugin = new webpack.DefinePlugin(
  {
    'process.env': {
      PORT: '"http://localhost:3000/"'
    }
  });

const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'build'),
};

module.exports = {

  entry: {
    app: PATHS.app,
  },
  resolve: {
    extensions: ['', '.js', '.css'],
  },
  output: {
    path: PATHS.build,
    filename: 'bundle.js',
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loaders: ['eslint'],
        include: PATHS.app,
      },
    ],
    loaders: [
      {
        test: /\.html$/,
        loaders: ['raw'],
        include: PATHS.app,
      },
      { test: /\.less$/, loader: 'style!css!less' },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader'),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['ng-annotate', 'babel?cacheDirectory,presets[]=es2015'],
        include: PATHS.app,
      },
      { test: /\.json$/, loader: 'json' },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './app/index.html' }),
    new ExtractTextPlugin('css/[name]-[hash].css'),
    definePlugin,
  ],
};
